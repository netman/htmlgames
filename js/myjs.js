const toggleCreditsPopUp = () => {
    document.querySelector('.credits')
        .classList.toggle('credherhid');
};

document.querySelector('#showcredpop')
    .addEventListener('click', toggleCreditsPopUp);

document.querySelector('#itzlink')
    .addEventListener('click', toggleCreditsPopUp);

const toggleLoginPopUp = () => {
    document.querySelector('.log')
        .classList.toggle('loghid');
};

document.querySelector('#showl')
    .addEventListener('click', toggleLoginPopUp);

document.querySelector('#itzlinq')
    .addEventListener('click', toggleLoginPopUp);